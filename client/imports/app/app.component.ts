import {Component} from "@angular/core";
import template from "./app.component.html";
import style from "./app.component.scss";
import {DemoCollection} from "../../../both/collections/demo.collection";

@Component({
    selector: "app",
    template,
    styles: [style]
})
export class AppComponent {
    constructor() {
    }

    createUser() {
        Accounts.createUser({"email": "foo@bar.com", "password": "foobar"}, err => {
            if (err) {
                console.log(err);
            } else {
                console.log("foobar created")
            }
        })
    }

    createDemo() {
        let newDemo: any = {
            name: "Rob",
            age: 17
        };

        DemoCollection.collection.insert(newDemo, (err)=>{
            if (err)
                console.log(err);
        });
    }
}
