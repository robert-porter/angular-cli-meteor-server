# Overview

- Server to go with https://bitbucket.org/robert-porter/angular-cli-meteor-client
- Based on Angular2-Meteor Boilerplate
- Originally created with meteor create --example angular2-boilerplate

# Dev Build/Run

1. git clone https://bitbucket.org/robert-porter/angular-cli-meteor-server
2. cd angular-cli-meteor-server
3. meteor npm install
3. meteor run

# Prod Build/Run

1. meteor build dist --architecture <<insert architecture>>
2. export MONGO_URL=....
3. export ROOT_URL=http://<<your server>>:3000/
4. export PORT=3000
5. cd meteor-server/bundle
6. node main.js