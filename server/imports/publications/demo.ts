import {DemoCollection} from "../../../both/collections/demo.collection";

Meteor.publish("DemoCollection", () => {
    return DemoCollection.find( {})
});